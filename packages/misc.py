import pickle
import os
import numpy as np
import tensorflow as tf

def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


#------------------------Log file-----------------------------------
def log_file(msg = "", pid="sample", addr="../TrainedNets/"):
    """maintains a log file

    Parameters
    ----------
    msg : str (or other type forced to str)
        message that is appended to the file
    pid : str
        contains the folder name; Here, process id --> hence 'pid'
    addr : str
        directory of the 'pid' folder
    """

    if type(msg)!=str:
        msg = str(msg)
    filepath = addr+pid+"/"
    if not os.path.isdir(filepath):
        os.mkdir(filepath)
    with open(filepath+'logfile.txt', 'a') as f:
        f.write(msg+'\n')
    return



#------------------------Dense to one hot encoding -----------------------------------------
def dense_to_one_hot(labels_dense, num_classes):
  """Convert class labels from scalars to one-hot vectors."""
  num_labels = labels_dense.shape[0]
  index_offset = np.arange(num_labels) * num_classes
  labels_one_hot = np.zeros((num_labels, num_classes))
  labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
  return labels_one_hot

#--------------------------------------------------------------------------------------------


def save_net(sess, x, y_conv, y_ = None, cross_entropy = None, accuracy=None, filename="trainedNet", pid="sample", addr="../TrainedNets/"):
    """Save graph and all hyperparementers (at the end)

    Parameters
    ----------
    sess : tensorflow Session()
    x, y_, y_conv, cross_entropy, accuracy : tensors and placeholders
        saved along with graph for reuse later
    filename : str
        desired name of graph file
    pid : str
        contains the folder name; Here, process id --> hence 'pid'
    addr : str
        directory of the 'pid' folder
    """

    tf.add_to_collection('x', x)
    tf.add_to_collection('y_', y_)
    tf.add_to_collection('y_conv', y_conv)
    tf.add_to_collection('cross_entropy', cross_entropy)
    tf.add_to_collection('accuracy', accuracy)
    saver = tf.train.Saver()
    filepath = addr+pid+"/"
    if not os.path.isdir(filepath):
        os.mkdir(filepath)
    save_path = filepath+filename+".ckpt"
    saver.save(sess, save_path)

def get_CIFAR_data(filename):
    """Acquires data and label from the given filename
    
    Parameters
    ----------
    filename: str
        filename with relative addressing
    """
    temp = unpickle(filename)
    data = temp[b'data']
    label = dense_to_one_hot(np.array(temp[b'labels']), 10)
    return data, label
