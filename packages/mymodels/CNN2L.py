import tensorflow as tf
from packages.mymodels.base import weight_variable, bias_variable
from packages.mymodels.base import conv2d, max_pool_2x2, batch_normalize



def graph(x, keep_prob):
    #Layer1
    W_conv1 = weight_variable([5, 5, 3, 32])
    b_conv1 = bias_variable([32])
    x_image = tf.reshape(x, [-1,32,32,3])
    h_conv1 = conv2d(x_image, W_conv1) + b_conv1
    h_conv1 = batch_normalize(h_conv1)
    h_conv1 = tf.nn.relu(h_conv1)
    h_pool1 = max_pool_2x2(h_conv1)
    
    #Layer2
    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])
    h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
    h_conv2 = batch_normalize(h_conv2)
    h_conv2 = tf.nn.relu(h_conv2)
    h_pool2 = max_pool_2x2(h_conv2)
    
    #Densely Connected Layer
    W_fc1 = weight_variable([8 * 8 * 64, 1024])
    b_fc1 = bias_variable([1024])
    h_pool2_flat = tf.reshape(h_pool2, [-1, 8*8*64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
    
    #Dropout
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
    
    #Readout Layer
    W_fc2 = weight_variable([1024, 10])
    b_fc2 = bias_variable([10])
    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
    
    return y_conv
