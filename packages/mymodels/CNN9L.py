import tensorflow as tf
from packages.mymodels.base import weight_variable, bias_variable
from packages.mymodels.base import conv2d, max_pool_2x2, batch_normalize



def graph(x, keep_prob):
    with tf.name_scope("ReshapeInput"):
        x_image = tf.reshape(x, [-1,3,32,32])
        x_image = tf.transpose(x_image, perm=[0,2,3,1])
    with tf.name_scope('Layer1'):
        W_conv1 = weight_variable([5, 5, 3, 8])
        b_conv1 = bias_variable([8])
        h_conv1 = conv2d(x_image, W_conv1) + b_conv1
        h_conv1 = batch_normalize(h_conv1)
        h_conv1 = tf.nn.relu(h_conv1)

        W_conv1b = weight_variable([5, 5, 8, 16])
        b_conv1b = bias_variable([16])
        h_conv1b = conv2d(h_conv1, W_conv1b) + b_conv1b
        h_conv1b = batch_normalize(h_conv1b)
        h_conv1b = tf.nn.relu(h_conv1b)

        W_conv1c = weight_variable([5, 5, 16, 32])
        b_conv1c = bias_variable([32])
        h_conv1c = conv2d(h_conv1b, W_conv1c) + b_conv1c
        h_conv1c = batch_normalize(h_conv1c)
        h_conv1c = tf.nn.relu(h_conv1c)
        h_pool1 = max_pool_2x2(h_conv1c)
    
    with tf.name_scope('Layer2'):
        W_conv2 = weight_variable([5, 5, 32, 48])
        b_conv2 = bias_variable([48])
        h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
        h_conv2 = batch_normalize(h_conv2)
        h_conv2 = tf.nn.relu(h_conv2)

        W_conv2b = weight_variable([5, 5, 48, 56])
        b_conv2b = bias_variable([56])
        h_conv2b = conv2d(h_conv2, W_conv2b) + b_conv2b
        h_conv2b = batch_normalize(h_conv2b)
        h_conv2b = tf.nn.relu(h_conv2b)

        W_conv2c = weight_variable([5, 5, 56, 64])
        b_conv2c = bias_variable([64])
        h_conv2c = conv2d(h_conv2b, W_conv2c) + b_conv2c
        h_conv2c = batch_normalize(h_conv2c)
        h_conv2c = tf.nn.relu(h_conv2c)
        h_pool2 = max_pool_2x2(h_conv2c)

    with tf.name_scope('Layer3'):
        W_conv3 = weight_variable([5, 5, 64, 96])
        b_conv3 = bias_variable([96])
        h_conv3 = conv2d(h_pool2, W_conv3) + b_conv3
        h_conv3 = batch_normalize(h_conv3)
        h_conv3 = tf.nn.relu(h_conv3)

        W_conv3b = weight_variable([5, 5, 96, 112])
        b_conv3b = bias_variable([112])
        h_conv3b = conv2d(h_conv3, W_conv3b) + b_conv3b
        h_conv3b = batch_normalize(h_conv3b)
        h_conv3b = tf.nn.relu(h_conv3b)

        W_conv3c = weight_variable([5, 5, 112, 128])
        b_conv3c = bias_variable([128])
        h_conv3c = conv2d(h_conv3b, W_conv3c) + b_conv3c
        h_conv3c = batch_normalize(h_conv3c)
        h_conv3c = tf.nn.relu(h_conv3c)
        h_pool3 = max_pool_2x2(h_conv3c)
    
    with tf.name_scope('DenselyConnested'):
        W_fc1 = weight_variable([4 * 4 * 128, 1024])
        b_fc1 = bias_variable([1024])
        h_pool3_flat = tf.reshape(h_pool3, [-1, 4*4*128])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool3_flat, W_fc1) + b_fc1)

        """W_fc1b = weight_variable([1024, 1024])
        b_fc1b = bias_variable([1024])
        h_fc1b = tf.nn.relu(tf.matmul(h_fc1a, W_fc1b) + b_fc1b)

        W_fc1c = weight_variable([1024, 1024])
        b_fc1c = bias_variable([1024])
        h_fc1 = tf.nn.relu(tf.matmul(h_fc1b, W_fc1c) + b_fc1c)"""
        
    with tf.name_scope('Dropout'):
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
        
    with tf.name_scope('Readout'):
        W_fc2 = weight_variable([1024, 10])
        b_fc2 = bias_variable([10])
        y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    tf.summary.image('summary_input', (x_image[:,:,:,0:3]))
    tf.summary.image('summary_hpool1', (h_pool1[:,:,:,0:3]))
    tf.summary.image('summary_hpool2', (h_pool2[:,:,:,0:3]))
    tf.summary.image('summary_hpool3', (h_pool3[:,:,:,0:3]))
    tf.summary.histogram("input", x_image)
    tf.summary.histogram("h_conv1", h_conv1)
    tf.summary.histogram("h_conv2", h_conv2)
    tf.summary.histogram("h_conv3", h_conv3)
    tf.summary.histogram("h_fc1", h_fc1)
    tf.summary.histogram("h_fc1_drop", h_fc1_drop)
    tf.summary.histogram("y_conv", y_conv)
    
    return y_conv
