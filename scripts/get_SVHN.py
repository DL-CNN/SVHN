import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib inline

def data_label(filename):
    """
    filename: str
    """
    train_data = sio.loadmat(filename)

    # access to the dict
    data = np.rollaxis(train_data['X'],3)
    label = train_data['y'].flatten() -1
    #one-hot encoding
    one_hot_label = np.zeros((len(label),10))
    one_hot_label[np.arange(len(label)),label] = 1
    return data, one_hot_label



if __name__=="__main__":
    image_ind = 10
    train_data = sio.loadmat('../dataset/train_32x32.mat')

    # access to the dict
    x_train = np.swapaxes(train_data['X'].T,2,3).flatten()
    y_train = train_data['y']

    # show sample
    plt.imshow(np.reshape(x_train,(-1,3,32,32))[image_ind,1,:,:])
    plt.show()
    print(y_train[image_ind])
    print(type(x_train), x_train.shape, y_train.shape)
