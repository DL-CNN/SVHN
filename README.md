# Street View House Number

### Description
["SVHN is a real-world image dataset for developing machine learning and object recognition algorithms with minimal requirement on data preprocessing and formatting.
It can be seen as similar in flavor to MNIST (e.g., the images are of small cropped digits),
but incorporates an order of magnitude more labeled data (over 600,000 digit images) and comes from a significantly harder,
unsolved, real world problem (recognizing digits and numbers in natural scene images).
SVHN is obtained from house numbers in Google Street View images".](http://ufldl.stanford.edu/housenumbers/)

The data is available in two formats, one contains full numbers with variable resolution, whereas the other contains, cropped numbers with fixed resolution.
For this work, the data with cropped numbers with fixed resolution have been used, as shown in the image below.

![alt text](http://ufldl.stanford.edu/housenumbers/32x32eg.png)

### Index
* [Structure of the repository](https://gitlab.com/DL-CNN/SVHN#structure-of-the-repository)
* [Results](https://gitlab.com/DL-CNN/SVHN#implementation-results)
* [Conclusion](https://gitlab.com/DL-CNN/SVHN#conclusion)


### Elaboration

#### Structure of the repository
* packages: contains module definitions created for this work
* scripts: .py scripts for training and evaluations
  * /logsTf: contains the tensorboard graph files, arranged as '/../train' and '/../test' directories
* TrainedNets: contains the log files for various evaluations
* dataset: contains the data files used for this work

#### Implementation & Results
* Training model
  The training model is an adaption the All CNN model proposed in the work by [Springenberg et al](https://arxiv.org/abs/1412.6806)
and the respective tensorboard visualization is given in the image below.

![alt text](/misc/model.png)
* Test Sample with a feature activation

![alt text](/misc/test_sample.png)
![alt text](/misc/feature1_activation.png)
* Overall accuracy

Training time ~11.5hrs

![alt text](/misc/overall_accuracy.png)
* Overall cross entropy loss

![alt text](/misc/overall_crossentropy_loss.png)

#### Conclusion
This work was aimed at developing our skills on classification based tasks on image data using CNN.
For this task an adaptation of the model named as [All CNN](https://arxiv.org/abs/1412.6806) has been implemented.
An important step is the use of batch normalization of each feature between every convolution and ReLU activations, more aptly refering to 'Feature Normalization'.

Another aim of this work is to use the same training architecture for multiple tasks (as has also been done for our [DLL-CNN:CIFAR10](https://gitlab.com/DL-CNN/CIFAR10) project)
and make sustainable improvements in the architecture (meaning, improvements that are relevant globally and not restricted to a specific problem).
In that respect, a comprehensive evaluation shall be offered as a valid conclusion of this work.



NOTE: set `pid=<string>` in train.py before performing evaluations, to store as the sub-directory in 'TrainedNets/' and 'scripts/logsTf/'
