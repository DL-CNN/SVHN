step size 0
Loss= 2.337314, Training Accuracy= 0.12015
Validation Loss= 2.338297, Validation Accuracy= 0.11516
step size 25
Loss= 2.197346, Training Accuracy= 0.24669
Validation Loss= 2.201790, Validation Accuracy= 0.23655
step size 50
Loss= 2.044151, Training Accuracy= 0.38431
Validation Loss= 2.047455, Validation Accuracy= 0.38288
step size 75
Loss= 1.880068, Training Accuracy= 0.52035
Validation Loss= 1.884670, Validation Accuracy= 0.51538
step size 100
Loss= 1.753400, Training Accuracy= 0.61655
Validation Loss= 1.757035, Validation Accuracy= 0.61616
step size 125
Loss= 1.612224, Training Accuracy= 0.73350
Validation Loss= 1.619684, Validation Accuracy= 0.72773
step size 150
Loss= 1.506425, Training Accuracy= 0.78410
Validation Loss= 1.513668, Validation Accuracy= 0.78319
step size 175
Loss= 1.440620, Training Accuracy= 0.79908
Validation Loss= 1.447948, Validation Accuracy= 0.79284
step size 200
Loss= 1.381227, Training Accuracy= 0.81868
Validation Loss= 1.389958, Validation Accuracy= 0.81236
step size 225
Loss= 1.334063, Training Accuracy= 0.83535
Validation Loss= 1.342205, Validation Accuracy= 0.83138
step size 250
Loss= 1.287949, Training Accuracy= 0.84959
Validation Loss= 1.296941, Validation Accuracy= 0.84484
step size 275
Loss= 1.257989, Training Accuracy= 0.85630
Validation Loss= 1.269040, Validation Accuracy= 0.85008
step size 300
Loss= 1.246125, Training Accuracy= 0.85741
Validation Loss= 1.257472, Validation Accuracy= 0.85194
step size 325
Loss= 1.211661, Training Accuracy= 0.86394
Validation Loss= 1.223275, Validation Accuracy= 0.85963
step size 350
Loss= 1.194181, Training Accuracy= 0.86677
Validation Loss= 1.204332, Validation Accuracy= 0.86132
step size 375
Loss= 1.180514, Training Accuracy= 0.86509
Validation Loss= 1.193622, Validation Accuracy= 0.85922
step size 400
Loss= 1.149287, Training Accuracy= 0.87634
Validation Loss= 1.162513, Validation Accuracy= 0.86996
step size 425
Loss= 1.135198, Training Accuracy= 0.88375
Validation Loss= 1.147592, Validation Accuracy= 0.87774
step size 450
Loss= 1.124129, Training Accuracy= 0.88295
Validation Loss= 1.136590, Validation Accuracy= 0.87674
step size 475
Loss= 1.111413, Training Accuracy= 0.88760
Validation Loss= 1.124801, Validation Accuracy= 0.87888
step size 500
Loss= 1.091121, Training Accuracy= 0.89791
Validation Loss= 1.104789, Validation Accuracy= 0.89034
Time elapsed: 19755 sec
step size 0
Loss= 2.341798, Training Accuracy= 0.12225
Validation Loss= 2.343817, Validation Accuracy= 0.11598
step size 25
Loss= 2.204998, Training Accuracy= 0.23546
Validation Loss= 2.208964, Validation Accuracy= 0.23146
step size 50
Loss= 2.061573, Training Accuracy= 0.36415
Validation Loss= 2.065671, Validation Accuracy= 0.36236
step size 75
Loss= 1.891532, Training Accuracy= 0.51386
Validation Loss= 1.893722, Validation Accuracy= 0.51178
step size 100
Loss= 1.754614, Training Accuracy= 0.62408
Validation Loss= 1.757326, Validation Accuracy= 0.62103
step size 125
Loss= 1.618960, Training Accuracy= 0.72587
Validation Loss= 1.624232, Validation Accuracy= 0.72181
step size 150
Loss= 1.514269, Training Accuracy= 0.77931
Validation Loss= 1.520419, Validation Accuracy= 0.77564
step size 175
Loss= 1.448534, Training Accuracy= 0.79315
Validation Loss= 1.455569, Validation Accuracy= 0.78688
step size 200
Loss= 1.375350, Training Accuracy= 0.81725
Validation Loss= 1.381981, Validation Accuracy= 0.81504
step size 225
Loss= 1.328283, Training Accuracy= 0.83763
Validation Loss= 1.335044, Validation Accuracy= 0.83506
step size 250
Loss= 1.285520, Training Accuracy= 0.84963
Validation Loss= 1.294285, Validation Accuracy= 0.84430
step size 275
Loss= 1.253150, Training Accuracy= 0.85758
Validation Loss= 1.263108, Validation Accuracy= 0.85176
step size 300
Loss= 1.245577, Training Accuracy= 0.85154
Validation Loss= 1.256694, Validation Accuracy= 0.84630
step size 325
Loss= 1.200837, Training Accuracy= 0.86905
Validation Loss= 1.211058, Validation Accuracy= 0.86414
step size 350
Loss= 1.183690, Training Accuracy= 0.87010
Validation Loss= 1.193678, Validation Accuracy= 0.86423
step size 375
Loss= 1.174448, Training Accuracy= 0.86445
Validation Loss= 1.187013, Validation Accuracy= 0.85954
step size 400
Loss= 1.145130, Training Accuracy= 0.87664
Validation Loss= 1.157855, Validation Accuracy= 0.87060
step size 425
Loss= 1.130393, Training Accuracy= 0.88584
Validation Loss= 1.141925, Validation Accuracy= 0.88020
step size 450
Loss= 1.118095, Training Accuracy= 0.88186
Validation Loss= 1.131624, Validation Accuracy= 0.87469
step size 475
Loss= 1.102550, Training Accuracy= 0.88838
Validation Loss= 1.116225, Validation Accuracy= 0.88002
step size 500
Loss= 1.088073, Training Accuracy= 0.89711
Validation Loss= 1.101326, Validation Accuracy= 0.89016
step size 525
Loss= 1.081959, Training Accuracy= 0.89276
Validation Loss= 1.096146, Validation Accuracy= 0.88561
step size 550
Loss= 1.069625, Training Accuracy= 0.89816
Validation Loss= 1.084737, Validation Accuracy= 0.89144
step size 575
Loss= 1.059668, Training Accuracy= 0.90166
Validation Loss= 1.075718, Validation Accuracy= 0.89380
step size 600
Loss= 1.056924, Training Accuracy= 0.90037
Validation Loss= 1.071085, Validation Accuracy= 0.89426
step size 625
Loss= 1.049932, Training Accuracy= 0.90121
Validation Loss= 1.067253, Validation Accuracy= 0.89085
step size 650
Loss= 1.037930, Training Accuracy= 0.90659
Validation Loss= 1.054222, Validation Accuracy= 0.89890
step size 675
Loss= 1.031151, Training Accuracy= 0.90458
Validation Loss= 1.047813, Validation Accuracy= 0.89499
step size 700
Loss= 1.021662, Training Accuracy= 0.90536
Validation Loss= 1.039700, Validation Accuracy= 0.89603
step size 725
Loss= 1.012504, Training Accuracy= 0.90989
Validation Loss= 1.031310, Validation Accuracy= 0.90090
step size 750
Loss= 1.005454, Training Accuracy= 0.91205
Validation Loss= 1.023957, Validation Accuracy= 0.90117
step size 775
Loss= 0.996943, Training Accuracy= 0.91425
Validation Loss= 1.015777, Validation Accuracy= 0.90340
step size 800
Loss= 1.000409, Training Accuracy= 0.90696
Validation Loss= 1.021088, Validation Accuracy= 0.89676
step size 825
Loss= 0.986958, Training Accuracy= 0.91435
Validation Loss= 1.006453, Validation Accuracy= 0.90495
step size 850
Loss= 0.981779, Training Accuracy= 0.91626
Validation Loss= 1.002408, Validation Accuracy= 0.90472
step size 875
Loss= 0.982560, Training Accuracy= 0.91533
Validation Loss= 1.004272, Validation Accuracy= 0.90404
step size 900
Loss= 0.973434, Training Accuracy= 0.91722
Validation Loss= 0.994542, Validation Accuracy= 0.90513
step size 925
Loss= 0.971327, Training Accuracy= 0.91835
Validation Loss= 0.993026, Validation Accuracy= 0.90686
step size 950
Loss= 0.969991, Training Accuracy= 0.91841
Validation Loss= 0.990548, Validation Accuracy= 0.90691
step size 975
Loss= 0.963053, Training Accuracy= 0.91784
Validation Loss= 0.984603, Validation Accuracy= 0.90677
step size 1000
Loss= 0.954441, Training Accuracy= 0.92262
Validation Loss= 0.976007, Validation Accuracy= 0.90959
step size 1025
Loss= 0.949103, Training Accuracy= 0.92151
Validation Loss= 0.971779, Validation Accuracy= 0.90909
step size 1050
Loss= 0.947781, Training Accuracy= 0.92638
Validation Loss= 0.970737, Validation Accuracy= 0.91269
step size 1075
Loss= 0.942944, Training Accuracy= 0.92455
Validation Loss= 0.966108, Validation Accuracy= 0.91396
step size 1100
Loss= 0.940848, Training Accuracy= 0.92537
Validation Loss= 0.965109, Validation Accuracy= 0.91228
